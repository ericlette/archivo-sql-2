-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: sistema_electronico
-- ------------------------------------------------------
-- Server version	5.7.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `buy_type`
--

DROP TABLE IF EXISTS `buy_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buy_type` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buy_type`
--

LOCK TABLES `buy_type` WRITE;
/*!40000 ALTER TABLE `buy_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `buy_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `buy_type_saf`
--

DROP TABLE IF EXISTS `buy_type_saf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buy_type_saf` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `buy_type_id` int(10) unsigned DEFAULT NULL,
  `saf_id` int(10) unsigned DEFAULT NULL,
  `maximum_amount` double(8,2) NOT NULL,
  `minimum_amount` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `buy_type_saf_saf_id_foreign` (`saf_id`),
  KEY `buy_type_saf_buy_type_id_foreign` (`buy_type_id`),
  CONSTRAINT `buy_type_saf_buy_type_id_foreign` FOREIGN KEY (`buy_type_id`) REFERENCES `buy_types` (`id`),
  CONSTRAINT `buy_type_saf_saf_id_foreign` FOREIGN KEY (`saf_id`) REFERENCES `saf` (`id_saf`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buy_type_saf`
--

LOCK TABLES `buy_type_saf` WRITE;
/*!40000 ALTER TABLE `buy_type_saf` DISABLE KEYS */;
INSERT INTO `buy_type_saf` VALUES (1,1,1,600.00,0.00,NULL,NULL),(2,2,1,1000.00,601.00,NULL,NULL);
/*!40000 ALTER TABLE `buy_type_saf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `buy_types`
--

DROP TABLE IF EXISTS `buy_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buy_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buy_types`
--

LOCK TABLES `buy_types` WRITE;
/*!40000 ALTER TABLE `buy_types` DISABLE KEYS */;
INSERT INTO `buy_types` VALUES (1,'Licitacion Publica',NULL,NULL),(2,'Licitacion Privada',NULL,NULL),(3,'Concurso de Precios',NULL,NULL),(4,'Menor Cuantía',NULL,NULL),(5,'Consultoría',NULL,NULL);
/*!40000 ALTER TABLE `buy_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clasif_presup`
--

DROP TABLE IF EXISTS `clasif_presup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clasif_presup` (
  `Ejercicio` smallint(4) DEFAULT NULL COMMENT 'Año del Ejercio',
  `Inciso` smallint(1) DEFAULT NULL COMMENT 'Inciso del Objeto',
  `Principal` smallint(1) DEFAULT NULL COMMENT 'Principal del Objeto',
  `Parcial` smallint(1) DEFAULT NULL COMMENT 'Parcial del Objeto',
  `Subparcial` smallint(1) DEFAULT NULL COMMENT 'Subparcial del Objeto',
  `Es Hoja` varchar(2) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Seleccionable o no el Objeto',
  `Muestra_Rubro_Prov` smallint(1) DEFAULT '0',
  `descripcion_corta` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Descripcion Abreviada del Objeto',
  `descripcion_larga` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Descripcion del Objeto',
  `Estado` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Estado del Objeto',
  `Fecha Baja` date DEFAULT NULL COMMENT 'Fecha de Baja del Objeto',
  `Fecha Alta` date DEFAULT NULL COMMENT 'Fecha de Alta del Objeto',
  `Usuario Ult. Modif.` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Usuario que Modifico el Objeto',
  `Fecha Ult. Modif.` date DEFAULT NULL COMMENT 'Fecha de Modificacion del Objeto',
  `id_clasif_presup` int(11) NOT NULL AUTO_INCREMENT,
  `Muestra_General` smallint(1) DEFAULT '1',
  `nube` mediumtext COLLATE utf8_spanish_ci,
  PRIMARY KEY (`id_clasif_presup`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clasif_presup`
--

LOCK TABLES `clasif_presup` WRITE;
/*!40000 ALTER TABLE `clasif_presup` DISABLE KEYS */;
INSERT INTO `clasif_presup` VALUES (2016,2,0,0,0,'No',0,'BIENES DE CONSUMO','BIENES DE CONSUMO','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',1,1,'Materiales y suministros consumibles para el funcionamiento de los entes estatales, incluidos los que se destinan a conservación y reparación de bienes de capital. Incluye la adquisición de bienes para su transformación y/o enajenación ulterior por aquellas entidades que desarrollan actividades de carácter comercial, industrial y/o servicios, o por dependencias u organismos que vendan o distribuyan elementos adquiridos con fines promocionales luego de su exhibición en exposiciones, ferias, etc. Las principales características que deben reunir los bienes comprendidos en este inciso son: que por su naturaleza estén destinados al consumo: final, intermedio, propio o de terceros, y que su tiempo de utilización sea relativamente corto, generalmente dentro del ejercicio. '),(2016,2,1,0,0,'No',1,'PROD.ALIMENT. AGROP. Y FORECT.','Productos alimenticios agropecuarios y forestales','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',2,1,'Bebidas y productos alimenticios, manufacturados o no, incluidos los animales vivos para consumo y para experimentación, aceites y grasas animales y vegetales; forrajes y otros alimentos para animales, productos agrícolas, ganaderos y de silvicultura, caza y pesca. Incluye el pago de gastos de comida, almuerzos o cenas de trabajo y el reintegro de erogaciones en concepto de racionamiento o sobre ración, liquidado de acuerdo con las normas vigentes.'),(2016,2,1,1,0,'Si',0,'ALIMENTOS PARA PERSONAS','Alimentos para personas','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',3,1,'Productos alimenticios, manufacturados o no, aceites y grasas animales y vegetales, bebidas en sus diversas formas y tabaco. Incluye el pago de gastos de comidas, almuerzos o cenas de trabajo y el reintegro de erogaciones de concepto de racionamiento o sobre-ración, liquidado de acuerdo a con las normas vigentes. Excluye los reintegros por gastos de comidas por horas extra abonadas al personal.'),(2016,2,1,2,0,'Si',0,'ALIMENTOS PARA ANIMALES','Alimentos para animales','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',4,1,'Forrajes y toda clase de alimentos para animales.'),(2016,2,1,3,0,'Si',0,'PRODUCTOS  PECUARIOS','Productos pecuarios','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',5,1,'Comprende el ganado y otros animales vivos destinados al consumo o para su uso científico. '),(2016,2,1,4,0,'Si',0,'PRODUCTOS AGROFORESTALES','Productos agroforestales','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',6,1,'Productos agroforestales tales como: goma laca, resinas y bálsamos, césped, árboles y arbustos.'),(2016,2,1,5,0,'Si',0,'MADERA, CORCHO Y SUS MANUFACTU','Madera, corcho y sus manufacturas','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',7,1,'Madera y corcho manufacturadas o no (excepto muebles), incluido carbón vegetal.'),(2016,2,1,9,0,'Si',0,'OTROS N.E.P.','Otros no especificados precedentemente','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',8,1,'Otros productos alimenticios, agropecuarios y forestales no especificados en las partidas anteriores.'),(2016,2,2,0,0,'No',1,'TEXTILES Y VESTUARIO','Textiles y vestuario','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',9,1,'Fibras y tejidos (animales, vegetales, sintéticos o artificiales) y confecciones de diversa índole. '),(2016,2,2,1,0,'Si',0,'HILADOS Y TELAS','Hilados y telas','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',10,1,'Fibras y tejidos animales, vegetales y sintéticos y artificiales.'),(2016,2,2,2,0,'Si',0,'PRENDAS DE VESTIR','Prendas de vestir','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',11,1,'Trajes, uniformes, calzados en sus distintos tipos, carteras, camisas, pantalones, medias, corbatas, guardapolvos, delantales, gorras y toda otra prenda de vestir no especificada precedentemente. '),(2016,2,2,3,0,'Si',0,'CONFECCIONES TEXTILES','Confecciones textiles','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',12,1,'Sábanas, fundas, frazadas, mantas, tapices, alfombras, cortinas, toallas y demás confecciones textiles no incluidas en el detalle precedente.'),(2016,2,2,9,0,'Si',0,'OTROS N.E.P.','Otros no especificados precedentemente','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',13,1,'Otros textiles y vestuarios no especificados precedentemente.'),(2016,2,3,0,0,'No',1,'PROD.  DE PAPEL, CARTON E MPRE','Productos de papel, carton e impresos','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',14,1,'Pulpa de madera, papel y cartón, envases y cajas de papel y cartón, productos de papel y cartón para oficinas, libros, revistas y periódicos, material de enseñanza, productos de papel y cartón para computación, imprenta y reproducción y otros productos de pulpa, papel y cartón. '),(2016,2,3,1,0,'Si',0,'PAPEL DE ESCRITORIO Y CARTON','Papel de escritorio y carton','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',15,1,'Papel y cartón en sus diversas formas y modalidades de uso común en oficinas. '),(2016,2,3,2,0,'Si',0,'PAPEL PARA COMPUTACION','Papel para computacion','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',16,1,'Papel y cartón en sus diversas formas y modalidades para uso en sistemas informáticos. '),(2016,2,3,3,0,'Si',0,'PROD. PREIMPRSOS Y ARTES GRAF.','Productos preimpresos y artes graficas','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',17,1,'Formularios, minutas, hojas de protocolo, planillas, folletos de cualquier índole, tarjetas, calendarios, partituras y demás productos preimpresos y artes gráficas. '),(2016,2,3,4,0,'Si',0,'PRODUCTOS DE PAPEL Y CARTON','Productos de papel y carton','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',18,1,'Papel y cartón en bobinas, en planchas y prensado; papel higiénico, pañuelos, toallas y servilletas; papel y cartón moldeado para uso doméstico (bandejas, platos, vasos, etc.); cartón y pasta de papel moldeado (canillas de bobinas, carretes y tapas);papel y cartón de filtro; papel engomado y adhesivo en sus diversas formas y otros productos de papel y cartón no enunciados precedentemente.'),(2016,2,3,5,0,'Si',0,'LIBROS, REVISTAS Y PERIODICOS','Libros, revistas y periodicos','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',19,1,'Libros, periódicos, revistas, folletos y publicaciones periódicas destinadas al consumo en oficinas públicas o para su distribución al público. No incluye los libros, revistas y otras publicaciones destinadas a la dotación de bibliotecas o de uso de los organismos.'),(2016,2,3,6,0,'Si',0,'TEXTOS DE ENSENANZA','Textos de ense├▒anza','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',20,1,'Artículos y materiales elaborados básicamente de papel y cartón destinados a la enseñanza, tales como libros para distribución o venta, guías de estudio, etc. '),(2016,2,3,7,0,'Si',0,'ESPECIES TIMBRADAS Y VALORES','Especies timbradas y valores','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',21,1,'Material impreso que, de acuerdo con la legislación vigente es utilizado como elemento de recaudación de ingresos fiscales, tales como: bandas de garantía, estampillas, papel sellado y otras especies para el mismo fin. '),(2016,2,3,9,0,'Si',0,'OTROS N.E.P.','Otros no especificados precedentemente','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',22,1,'Productos de papel, cartón e impresos no incluidos en las partidas parciales anteriores. '),(2016,2,4,0,0,'No',1,'PRODUCTOS DE CUERO Y CAUCHO','Productos de cuero y caucho','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',23,1,'Cueros crudos y cueros en sus distintos tipos, elaborados o semielaborados, hules y similares y caucho en sus distintas elaboraciones.'),(2016,2,4,1,0,'Si',0,'CUEROS Y PIELES','Cueros y pieles','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',24,1,'Cueros crudos, curtidos y adobados, cueros gamuzados y apergaminados, charol y cueros metalizados; pieles finas adobadas y adobadas sin depilar; y pieles en: planchas, tiras y paños para pulimento industrial.'),(2016,2,4,2,0,'Si',0,'ARTICULOS DE CUERO','Articulos de cuero','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',25,1,'Tientos, correas, suelas, arneses, maletas, bolsones, cintos, guantes, portafolios, estuches y demás artículos elaborados con cuero no especificado precedentemente. Se excluye calzado, prendas de vestir y carteras que están clasificados en la partida 2.2.2.Prendas de vestir.'),(2016,2,4,3,0,'Si',0,'ARTICULOS DE CAUCHO','Articulos de caucho','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',26,1,'Artículos de caucho acabado y semiacabados, vulcanizados y sin vulcanizar, incluyendo entre otros: láminas, tiras, varillas, perfiles, tubos, caños, mangueras, correas y cintas transportadoras, artículos higiénicos y farmacéuticos, revestimiento de pisos, flotadores, etc. '),(2016,2,4,4,0,'Si',0,'CUBIERTAS Y CAMARAS DE AIRE','Cubiertas y camaras de aire','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',27,1,'Cubiertas neumáticas sólidas y mullidas; cubiertas para vehículos y equipos distintos de los de circulación por carreteras, como aeronaves y topadoras, y para juguetes, muebles y otros usos. Cámaras para las cubiertas descriptas anteriormente. '),(2016,2,4,9,0,'Si',0,'OTROS N.E.P.','Otros no especificados precedentemente','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',28,1,'Otros productos de cuero y caucho no especificados precedentemente. '),(2016,2,5,0,0,'No',1,'PROD.QUIMICOS, COMBUS. Y LUBR.','Productos quimicos, combustibles y lubricantes','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',29,1,'Drogas, abonos, fertilizantes, plaguicidas y demás productos químicos y medicinales y productos de ramas industriales conexas (pinturas, barnices, fósforos, etc.). Combustibles en general (excepto petróleo crudo y gas natural) y aceites y grasas lubricantes. '),(2016,2,5,1,0,'Si',0,'COMPUESTOS  QUIMICOS','Compuestos quimicos','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',30,1,'Gases industriales; aire líquido y comprimido, acetileno y gases refrigerante, ácidos inorgánicos, álcalis y otros compuestos inorgánicos. Sustancias químicas orgánicas básicas; alcoholes acíclicos, fenoles: cetonas y quinonas. Otros elementos químicos, isótopos y compuestos radioactivos para combustible nuclear.'),(2016,2,5,2,0,'Si',0,'PRODUCTOS FARMACEUTICOS Y MEDI','Productos farmaceuticos y medicinales','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',31,1,'Preparados farmacéuticos para uso médico; preparados genéricos y de marcas registradas, ampollas, cápsulas, tabletas, ungüentos, productos botánicos pulverizados o molidos o preparados de otra forma; apósitos quirúrgicos, guatas medicinales, vendajes para fracturas y productos para sutura. Sustancias químicas utilizadas en la preparación de productos farmacéuticos; preparados para la higiene bucal y dental. '),(2016,2,5,3,0,'Si',0,'ABONOS Y FERTILIZANTES','Abonos y fertilizantes','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',32,1,'Abonos nitrogenados, fosfatados y potásicos puros, mixtos, compuestos y complejos, entre otros: urea, ácido nítrico, amoníaco, cloruro de amonio comercial y nitrato de potasio.'),(2016,2,5,4,0,'Si',0,'INSECTICIDAS, FUMIGANTES Y OTR','Insecticidas, fumigantes y otros','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',33,1,'Insecticidas, raticidas, fungicidas, plaguicidas, herbicidas, productos antigerminantes, desinfectantes y otros productos químicos de similares características y usos.'),(2016,2,5,5,0,'Si',0,'TINTAS, PINTURAS Y COLORANTES','Tintas, pinturas y colorantes','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',34,1,'Tintas para escribir, dibujar y para imprenta. Pinturas, barnices, esmaltes y lacas; pigmentos preparados y colores preparados; masillas y preparados similares no refractarios, para relleno y\' enlucido; disolventes, diluyentes y removedores de pintura. '),(2016,2,5,6,0,'Si',0,'COMBUSTIBLES Y LUBRICANTES','Combustibles y lubricantes','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',35,1,'Combustibles líquidos, nucleares, gas envasado, aceites de alumbrado y aceites y grasa lubricantes.'),(2016,2,5,7,0,'Si',0,'ESPECIFICOS VETERINARIOS','Especificos veterinarios','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',36,1,'Medicamentos y productos farmacéuticos y demás artículos para uso veterinario.'),(2016,2,5,8,0,'Si',0,'PRODUCTOS DE MATERIAL PLASTICO','Productos de material plastico','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',37,1,'Artículos diversos de material plástico, tales como: laminas, lienzos, bolsas, tubos y accesorios de PVC, etc. Se excluyen los artículos de material plástico clasificado en otros bienes de consumo.'),(2016,2,5,9,0,'Si',0,'OTROS N.E.P.','Otros no especificados precedentemente','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',38,1,'Explosivos, productos de pirotecnia, pólvoras propulsoras, mechas y detonadores y de seguridad, fuegos artificiales, bengalas, etc. Productos fotoquímicos como placas fotográficas, películas, papel sensibilizado y preparados químicos de uso fotográfico. Preparados aromáticos de uso personal como perfumes, aguas de colonia y de tocador; preparados de belleza y de maquillajes. Productos diversos derivados del petróleo excluidos los combustibles y lubricantes. Demás productos químicos no clasificados en las partidas parciales anteriores. '),(2016,2,6,0,0,'No',1,'PRODUCTOS DE MINERALES NO MET├ƒ','Productos de minerales no metalicos','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',39,1,'Artículos de cerámica, de vidrio, de loza y porcelana y de cemento; cal, yeso y asbesto y demás productos elaborados con minerales no metálicos. '),(2016,2,6,1,0,'Si',0,'PRODUCTOS DE ARCILLA Y CERAMIC','Productos de arcilla y ceramica','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',40,1,'Artículos de cerámica no refractaria, excepto los de loza y porcelana. Artículos de cerámica refractaria como ladrillos, bloques, tejas, losetas y otros similares para la construcción y los utilizados en la industria química y la industria en general, como así también cerámica utilizada en la agricultura. '),(2016,2,6,2,0,'Si',0,'PRODUCTOS DE VIDRIO','Productos de vidrio','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',41,1,'Vidrio en barra, en varillas, en bolas y tubos, colado o laminado, estirado o rollado; vidrios y cristales, lunas o vidrios de seguridad, espejos y envolturas tubulares de vidrio, vidrios para relojes, vidrios ópticos y piezas de vidrio óptico sin labrar, y demás productos de vidrio no especificado precedentemente. '),(2016,2,6,3,0,'Si',0,'PRODUCTOS DE LOZA Y PORCELANA','Productos de loza y porcelana','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',42,1,'Artículos de loza y porcelana para diversos usos como ser inodoros, lavamanos, mingitorios, etc.'),(2016,2,6,4,0,'Si',0,'PRODUCTOS DE CEMENTO, ASBESTO','Productos de cemento, asbesto y yeso','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',43,1,'Artículos de hormigón, cemento y yeso para uso en la construcción como losetas, baldosas, ladrillos, planchas, láminas, tableros, tubos y postes. Artículos de asbestocemento, fibrocemento de celulosa y de materiales similares. '),(2016,2,6,5,0,'Si',0,'CEMENTO, CAL Y YESO','Cemento, cal y yeso','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',44,1,'Cementos hidráulicos, incluso cemento de portland, cemento aluminoso y cemento hipersulfatado en forma clínica y en otras formas. Cal viva, cal apagada y cal hidráulica. Yesos producidos con yeso calcinado y con sulfato de calcio. '),(2016,2,6,9,0,'Si',0,'OTROS N.E.P.','Otros no especificados precedentemente','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',45,1,'Otros productos de minerales no metálicos como ser: hilados, telas y artículos de asbesto; materiales aislante de origen mineral (lana de escorias, vermiculita, etc.); productos de lana de vidrio para aislante térmico; piedras de amolar y productos abrasivos y otros artículos elaborados con sustancias minerales no metálicas no incluidas en las partidas parciales anteriores.'),(2016,2,7,0,0,'No',1,'PRODUCTOS METALICOS','Productos metalicos','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',46,1,'Productos metálicos sin elaborar y semielaborados y sus manufacturas. '),(2016,2,7,1,0,'Si',0,'PRODUCTOS FERROSOS','Productos ferrosos','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',47,1,'Hierro y acero en lingotes; tochos, barras, palastros y otras formas de hierro, acero y acero de aleación en estado semiacabado; hojas, planchas y rollos, barras y varillas, perfiles, alambre, tubos, caños y demás productos de hierro y acero.'),(2016,2,7,2,0,'Si',0,'PRODUCTOS NO FERROSOS','Productos no ferrosos','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',48,1,'Oro, plata, platino, cobre, plomo, cromo, manganeso, zinc, aluminio, níquel y estaño y otros metales comunes no ferrosos y sus aleaciones, sin elaborar, semielaborados y sus manufacturas.'),(2016,2,7,3,0,'Si',0,'MATERIAL DE GUERRA','Material de guerra','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',49,1,'Municiones, cascos, cartuchos, herramientas de campaña y afines, etc. '),(2016,2,7,4,0,'Si',0,'ESTRUCTURAS METALICAS ACABADAS','Estructuras metalicas acabadas','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',50,1,'Estructuras metálicas y sus partes, estructuras elaboradas en acero y productos similares, tales como: columnas, vigas, armaduras, puertas, ventanas y sus marcos, postigos, carpintería metálica utilizada en la construcción, rejas, celosías, etc. '),(2016,2,7,5,0,'Si',0,'HERRAMIENTAS MENORES','Herramientas menores','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',51,1,'Herramientas de mano del tipo utilizadas en agricultura, ganadería, horticultura, silvicultura, carpintería, chapistería, y otras industrias, tales como sierras y hojas de sierra, cuchillas y cizallas para máquinas y aparatos metálicos, destornilladores, picos, palas, tenazas, martillos, etc.'),(2016,2,7,9,0,'Si',0,'OTROS N.E.P.','Otros no especificados precedentemente','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',52,1,'Artículos de metal para uso doméstico (excepto utensilios de cocina y comedor), cerraduras, candados, pasadores, llaves y otros accesorios para edificios, muebles, vehículos y otros usos. Demás productos metálicos no incluidos en las partidas parciales anteriores. '),(2016,2,8,0,0,'No',1,'MINERALES','Minerales','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',53,1,'Minerales sólidos, minerales metalíferos, petróleo crudo y gas natural, piedras y arena. '),(2016,2,8,1,0,'Si',0,'MINERALES METALIFEROS','Minerales metaliferos','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',54,1,'Minerales de hierro (hematita, magnetita, limonita, siderosa y tocanita). Minerales no ferrosos (bauxita, cobalto, molibdeno, tántalo y vanadio).'),(2016,2,8,2,0,'Si',0,'PETROLEO CRUDO Y GAS NATURAL','Petroleo crudo y gas natural','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',55,1,'Petróleo crudo y gas natural en bruto.'),(2016,2,8,3,0,'Si',0,'CARBON MINERAL','Carbon mineral','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',56,1,'Carbón de piedra, antracita, carbones bituminosos y otros tipos de carbón mineral. Lignito y turba.'),(2016,2,8,4,0,'Si',0,'PIEDRA, ARCILLA Y ARENA','Piedra, arcilla y arena','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',57,1,'Piedras, arcilla y arenas, corindón natural, granito, mármoles y demás piedras calcáreas, canto rodado, pedregullo y arenas naturales de todo tipo y clase.'),(2016,2,8,9,0,'Si',0,'OTROS N.E.P.','Otros no especificados precedentemente','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',58,1,'Uranio, torio, talco, dolomita, azufre, y otros minerales no incluidos en las partidas parciales anteriores.'),(2016,2,9,0,0,'No',1,'OTROS BIENES DE CONSUMO','Otros bienes de consumo','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',59,1,'Otros bienes de consumo utilizados en oficinas, establecimiento de enseñanza, cocina y comedores, establecimientos hospitalarios y laboratorios, como así también materiales eléctricos y de limpieza no incluidos en otras partidas.'),(2016,2,9,1,0,'Si',0,'ELEMENTOS DE LIMPIEZA','Elementos de limpieza','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',60,1,'Jabones, detergentes y aguas lavandinas en todas sus formas. Betunes, ceras y cremas para calzados, pisos, y carrocerías, vidrios y metal. Preparados para desodorizar ambientes etc. Elementos o utensilios de limpieza como ser: Cepillos, trapos en sus diversas formas, plumeros, secadores, escobas, escobillones, baldes, palanganas, etc. demás elementos de limpieza no incluidos en otras partidas.'),(2016,2,9,2,0,'Si',0,'UTILES DE ESCRIT. OFIC.Y ENSE├æ','Utiles de escritorio, oficina y ense├▒anza','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',61,1,'Lápices, lapiceras, carpetas, reglas, punteros, broches, alfileres, abrochadoras, perforadoras, cintas impregnadas, disquetes, pegamentos, gomas de borrar, borradores, sellos, biblioratos, tizas, marcadores, etc., y demás elementos de uso común en oficinas y establecimientos de enseñanza no incluidos en ninguna otra partida.'),(2016,2,9,3,0,'Si',0,'UTILES Y MATERIALES ELECTRICOS','Utiles y materiales electricos','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',62,1,'Cables, llaves, interruptores, fichas, transformadores, lámparas, tubos fluorescentes, zócalos, arrancadores, pilas, acumuladores eléctricos y demás útiles y materiales eléctricos no incluidos en otras partidas.'),(2016,2,9,4,0,'Si',0,'UTENSILIOS DE COCINA Y COMEDOR','Utensilios de cocina y comedor','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',63,1,'Sartenes, cacerolas, artículos de cuchillería, cucharas, tenedores, cucharones, cernidores y tamices, espumaderas, pinzas para servir, saleros y pimenteros, tijera de trinchar, coladores, vasos, tazas, pocillos, platos y demás utensilios de cocina y comedor.'),(2016,2,9,5,0,'Si',0,'UT. MEN. MED.QUIR. Y DE LAB.','Utiles menores medico, quirurgico y de laboratorio','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',64,1,'Instrumental menor de uso práctico y científico en medicina, cirugía, odontología, veterinaria y laboratorio, tales como: jeringas, agujas, gasas, vendajes, material de sutura, guantes para cirugía, vasos de precipitación, pipetas, alambiques, curetas, pinzas, etc.'),(2016,2,9,6,0,'Si',0,'REPUESTOS Y ACCESORIOS','Repuestos y accesorios','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',65,1,'Repuestos y accesorios menores considerados como equipo instrumental complementario de maquinas y equipo. Comprende repuestos y accesorios para: máquinas de oficina en general, equipo de tracción, transporte y elevación, maquinaria y equipo de producción, equipos de computación, etc.'),(2016,2,9,9,0,'Si',0,'OTROS N.E.P.','Otros no especificados precedentemente','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',66,1,'Otros bienes de consumo no incluidos en las partidas anteriores.'),(2016,3,0,0,0,'No',0,'SERVICIOS NO PERSONALES','SERVICIOS NO PERSONALES','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',67,1,'Servicios para el funcionamiento de los entes estatales incluidos los que se destinan a conservación y reparación de bienes de capital. Incluye asimismo los servicios utilizados en los procesos productivos, por las entidades que desarrollan actividades de carácter comercial, industrial o servicio. Comprende: servicios básicos, arrendamientos de edificios, terrenos y equipos, servicios de mantenimiento, limpieza y reparación, servicios técnicos y profesionales, publicidad e impresión, servicios comerciales y financieros, etc.'),(2016,3,1,0,0,'No',1,'SERVICIOS BASICOS','Servicios basicos','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',68,1,'Servicios de provisión de electricidad, gas, agua (incluida la evacuación del efluente cloacal) y de comunicaciones Comprende el pago de:'),(2016,3,1,1,0,'Si',0,'ENERGIA ELECTRICA','Energia electrica','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',69,0,''),(2016,3,1,2,0,'Si',0,'AGUA Y CLOACAS','Agua y cloacas','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',70,0,''),(2016,3,1,3,0,'Si',0,'GAS','Gas','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',71,0,''),(2016,3,1,4,0,'Si',0,'T.T. Y T.I.CORREO ELEC.  R Y R','Telefonos, telex, telefax, internet, correo electronico, radiomensaje y radiotelecomunicaciones','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',72,1,''),(2016,3,1,5,0,'Si',0,'CORREOS Y TELEGRAFO','Correos y telegrafo','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',73,1,''),(2016,3,1,9,0,'Si',0,'OTROS N.E.P.','Otros no especificados precedentemente','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',74,1,''),(2016,3,2,0,0,'No',1,'ALQUILERES Y DERECHOS','Alquileres y derechos','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',75,1,'Arrendamiento de toda clase de bienes inmuebles, muebles y semovientes. Incluye asimismo el pago de derechos sobre los bienes intangibles, tales como los que se realizan en concepto de licencias por el uso de programas de computación y las sumas pactadas por la suscripción de contratos de “leasing” o la construcción, a riesgo del contratista, de obras bajo la modalidad “llave en mano”.'),(2016,3,2,1,0,'Si',0,'ALQUILER DE EDIFICIOS Y LOCALE','Alquiler de edificios y locales','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',76,1,''),(2016,3,2,2,0,'Si',0,'ALQ DE MAQ.EQ Y MED. DE TRASP.','Alquiler de maquinaria, equipo y medios de transporte','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',77,1,''),(2016,3,2,3,0,'Si',0,'ALQUILER DE EQUIPOS DE COMPUTA','Alquiler de equipos de computacion','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',78,1,''),(2016,3,2,4,0,'Si',0,'ALQUILER DE FOTOCOPIADORAS','Alquiler de fotocopiadoras','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',79,1,''),(2016,3,2,5,0,'Si',0,'ARRENDAMIENTO DE TIERRAS Y TER','Arrendamiento de tierras y terrenos','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',80,1,''),(2016,3,2,6,0,'Si',0,'DERECHOS DE BIENES INTANGIBLES','Derechos de bienes intangibles','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',81,1,''),(2016,3,2,7,0,'Si',0,'ALQUILER CON OPCI├ôN DE COMPRA','Alquiler con opcion a compra','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',82,1,''),(2016,3,2,9,0,'Si',0,'OTROS N.E.P.','Otros no especificados precedentemente','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',83,1,''),(2016,3,3,0,0,'No',1,'MANTENIMIENTO, REPAR. Y LIMP','Mantenimiento, reparacion y limpieza','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',84,1,'Servicios prestados para funcionamiento dentro del régimen de contratos de suministros para limpieza, desinfección, conservación y reparación, tales como: pintura, refacciones y mantenimiento.'),(2016,3,3,1,0,'Si',0,'MANT .Y REP. DE ED. Y LOC.','Mantenimiento y reparacion de edificios y locales','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',85,1,'Servicios de conservaciones y reparaciones de inmuebles que tiende a conservarlos en condiciones adecuadas de funcionamiento, tales como: pintura, desmonte y mantenimiento menores, incluye el pago de expensas comunes en  edificios o locales en propiedad horizontal.'),(2016,3,3,2,0,'Si',0,'MANT. Y REP. DE VEHECULOS','Mantenimiento y reparacion de vehiculos','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',86,1,'Servicio de conservación y reparación de vehículos en general.'),(2016,3,3,3,0,'Si',0,'MANT. Y REP.DE MAQ. Y EQUIP.','Mantenimiento y reparacion de maquinaria y equipos','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',87,1,'Servicios de mantenimiento y reparaciones menores de equipos y maquinarias a efectos de su normal funcionamiento.'),(2016,3,3,4,0,'Si',0,'MANT.Y REP.DE VIAS DE COMUN.','Mantenimiento y reparacion de vias de comunicacion','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',88,1,'Gastos destinados al mantenimiento y conservación de caminos, carreteras, autopistas, puentes y vías férreas, fluviales, aeródromos y otros.'),(2016,3,3,5,0,'Si',0,'LIMPIEZA, ASEO Y FUMIGACION','Limpieza, aseo y fumigacion','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',89,1,'Gastos por extracción de basura, limpieza, desinfección de edificios y oficinas públicas  y fumigación en general.'),(2016,3,3,6,0,'Si',0,'MANTENIMIENTO DE SISTEMAS INFO','Mantenimiento de sistemas informaticos','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',90,1,'Gastos destinados al mantenimiento y actualización del software correspondiente a los sistemas informáticos.'),(2016,3,3,9,0,'Si',0,'OTROS N.E.P.','Otros no especificados precedentemente','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',91,1,'Otros gastos de mantenimiento, reparación y limpieza no especificados precedentemente. '),(2016,3,4,0,0,'No',1,'SERVICIOS PROFESIONALES,TECNIC','Servicios profesionales, tecnicos y otros','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',92,1,'Honorarios legales o convencionales a peritos profesionales universitarios, especialistas, técnicos y otros, sin relación de dependencia y los servicios de consultoría y asesoría prestados por terceros relacionados con estudios, investigaciones, análisis, auditorías, sistemas computarizados, etc.'),(2016,3,4,1,0,'Si',0,'EST. INVEST.Y PROY. DE FACT.','Estudios, investigaciones y proyectos de factibilidad','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',93,1,''),(2016,3,4,2,0,'Si',0,'MEDICOS Y SANITARIOS','Medicos y sanitarios','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',94,1,''),(2016,3,4,3,0,'Si',0,'JURIDICOS','Juridicos','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',95,1,''),(2016,3,4,4,0,'Si',0,'CONTABILIDAD Y AUDITORIA','Contabilidad y auditoria','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',96,1,''),(2016,3,4,5,0,'Si',0,'DE CAPACITACION','De capacitacion','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',97,1,''),(2016,3,4,6,0,'Si',0,'DE INFORMATICA Y SISTEMAS COMP','De informatica y sistemas computarizados','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',98,1,''),(2016,3,4,7,0,'Si',0,'DE TURISMO','De turismo','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',99,1,''),(2016,3,4,8,0,'Si',0,'GERIATRICOS','Geriatricos','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',100,1,''),(2016,3,4,9,0,'Si',0,'OTROS N.E.P.','Otros no especificados precedentemente','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',101,1,''),(2016,3,5,0,0,'No',1,'SERVICIOS COMERCIALES Y FINANC','Servicios comerciales y financieros','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',102,1,'Servicios de transporte (terrestre, fluvial, marítimo o aéreo) de personas, bienes muebles y semovientes; servicios de peaje, servicios portuarios, de estibaje y almacenamiento. Erogaciones originadas en las ediciones y publicaciones que se realicen y en los servicios de impresión y reproducción. Primas y gastos de seguros y comisiones a bancos u otras entidades oficiales o privadas, etc. '),(2016,3,5,1,0,'Si',0,'TRANSPORTE','Transporte','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',103,1,'Fletes terrestres, marítimos y aéreos, así como el gasto derivado del embalaje de mercancías. Incluye asimismo el servicio de mudanzas. Incluye las erogaciones originadas en el transporte de efectos personales de agentes del Estado desde y hacia el exterior, de acuerdo con las normas legales vigentes y el transporte de personas por razones de servicios. '),(2016,3,5,2,0,'Si',0,'ALMACENAMIENTO','Almacenamiento','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',104,1,'Servicios de almacenajes de insumos, bienes muebles y otros materiales.'),(2016,3,5,3,0,'Si',0,'IMPRENTA, PUBLICACIONES Y REPR','Imprenta, publicaciones y reproducciones','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',105,1,'Servicios de impresión, copia, encuadernación prestados por terceros. Publicaciones de carácter informativo o divulgaciones de tipos culturales, científicos y técnicos. Se excluyen la impresión de formularios, libros y demás impresos destinados al uso de las oficinas que se imputarán a la partida 2.3.3 Productos de artes gráficas. '),(2016,3,5,4,0,'Si',0,'PRIMAS Y GASTOS DE SEGUROS','Primas y gastos de seguros','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',106,1,'Primas y gastos de seguros de personas o de cosas.'),(2016,3,5,5,0,'Si',0,'COMISIONES Y GASTOS BANCARIOS','Comisiones y gastos bancarios','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',107,1,'Gastos para atender los servicios que prestan los bancos, tales como: cobro de impuestos, apertura de carta de crédito, transferencias bancarias, etc.'),(2016,3,5,6,0,'Si',0,'COMISIONES Y GASTOS VENTA JUEG','Comisiones y gastos venta juegos','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',108,1,''),(2016,3,5,8,0,'Si',0,'COMISIONES POR VENTAS Y COBRAN','Comisiones por ventas y cobranzas','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',109,1,''),(2016,3,5,9,0,'Si',0,'OTROS N.E.P.','Otros no especificados precedentemente','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',110,1,'Otros servicios comerciales y financieros no especificados precedentemente'),(2016,3,6,0,0,'Si',0,'PUBLICIDAD Y PROPAGANDA','Publicidad y propaganda','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',111,1,'Gastos en concepto de publicidad y propaganda por medio de radiodifusoras, televisión, cines, teatros, periódicos, revistas, folletos, carteles, etc. Incluye los contratos con las agencias publicitarias y productoras cinematográficas y televisivas. '),(2016,3,7,0,0,'No',1,'PASAJES, VIATICOS Y OTROS','Pasajes, viaticos y otros','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',112,0,'Asignaciones que se otorgan al personal con motivo de la prestación de servicio fuera de lugar habitual de trabajo, conforme a las normas y reglamentos vigentes incluyendo los gastos de pasajes pagados a los agentes y/o empresas prestadoras del servicio. '),(2016,3,7,1,0,'Si',0,'PASAJES','Pasajes','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',113,0,'Comprende los pasajes pagados a empresas de transporte por el traslado del personal de las instituciones.'),(2016,3,7,2,0,'Si',0,'VIATICOS','Viaticos','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',114,0,'Asignaciones que se otorgan al personal con motivo de la prestación de servicios fuera del lugar habitual de trabajo, conforme a la legislación vigente. Excluye los importes abonados al personal en concepto de movilidad. '),(2016,3,7,9,0,'Si',0,'OTROS N.E.P.','Otros no especificados precedentemente','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',115,0,''),(2016,3,8,0,0,'No',1,'IMPUESTOS,DERECHOS,TASAS Y JUI','Impuestos, derechos, tasas y juicios','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',116,0,'Impuestos, derechos, tasas, regalías, multas, recargos, juicios y mediaciones judiciales.'),(2016,3,8,1,0,'Si',0,'IMPUESTOS INDIRECTOS','Impuestos indirectos','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',117,0,'Gastos destinados al pago de impuestos indirectos.'),(2016,3,8,2,0,'Si',0,'IMPUESTOS DIRECTOS','Impuestos directos','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',118,0,'Gastos destinados al pago de impuestos directos.'),(2016,3,8,3,0,'Si',0,'DERECHOS Y TASAS','Derechos y tasas','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',119,0,'Gastos destinados al pago de derechos y tasas. '),(2016,3,8,4,0,'Si',0,'MULTAS Y RECARGOS','Multas y recargos','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',120,0,'Gastos en concepto de multas y recargos, cualquiera fuese el origen del pago a realizar.'),(2016,3,8,5,0,'Si',0,'REGALIAS','Regalias','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',121,0,'Gastos por la transferencia del dominio, uso o goce de cosas o la de cesión de derechos a favor de la entidad, cuyo monto se determine con relación a una unidad de producción, de venta, de explotación, etc. cualquiera sea la denominación asignada a su beneficiario. '),(2016,3,8,6,0,'Si',0,'JUICIOS Y MEDIACIONES','Juicios y mediaciones','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',122,0,'Gastos originados en sentencias judiciales contra el Estado y en acuerdos de partes resultantes de procesos de mediación en donde éste deba hacerse cargo. Incluye las costas y demás gastos generados en el proceso judicial.'),(2016,3,8,9,0,'Si',0,'OTROS N.E.P.','Otros no especificados precedentemente','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',123,0,'Otros impuestos, derechos y tasas no especificadas precedentemente '),(2016,3,9,0,0,'No',1,'OTROS SERVICIOS','Otros servicios','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',124,1,'Gastos en servicios no personales no especificados en las partidas anteriores, tales como servicio de ceremonial (recepciones, homenajes, agasajos y similares), gastos reservados, protocolares y servicios varios.'),(2016,3,9,1,0,'Si',0,'SERVICIOS DE CEREMONIAL','Servicios de ceremonial','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',125,1,'Gastos destinados al servicio de ceremonial como es el caso de eventos, recepciones, homenajes, agasajos y similares. '),(2016,3,9,2,0,'Si',0,'GASTOS RESERVADOS','Gastos reservados','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',126,0,''),(2016,3,9,3,0,'Si',0,'SERVICIOS DE VIGILANCIA','Servicios de vigilancia','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',127,1,'Gastos destinados a la seguridad de bienes y personas.'),(2016,3,9,4,0,'Si',0,'GASTOS PROTOCOLARES','Gastos protocolares','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',128,1,'Gastos en concepto de reintegros que se abonan a autoridades superiores para afrontar los gastos protocolares que el ejercicio de la función les requiera.'),(2016,3,9,5,0,'Si',0,'DEMOLICI├ôN DE EDIFICIOS','Demolicion de edificios','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',129,1,''),(2016,3,9,6,0,'Si',0,'PASANTIAS','Pasantias','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',130,0,'Gastos originados en convenios con universidades por pasantías que estudiantes y graduados cumplen en las jurisdicciones y entidades y que en razón de ello no suponen la contraprestación de servicios de carácter personal, no obstante implica la contraprestación de servicios. '),(2016,3,9,7,0,'Si',0,'BECAS DE INVESTIGACION','Becas de investigacion','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',131,0,'Gastos originados en estipendios que se abonan en concepto de trabajos de investigación científica y tecnológica sin implicar relación laboral actual o futura. La contraprestación del estipendio supone el cumplimiento de un trabajo convenido y la presentación de los respectivos informes.'),(2016,3,9,8,0,'Si',0,'PECULIO','Peculio','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',132,0,'Gastos originados en las retribuciones a internos en establecimientos penales, educativos y médicos asistenciales por las actividades laborales que se realizan en distintos talleres de laborterapia.'),(2016,3,9,9,0,'Si',0,'OTROS N.E.P.','Otros no especificados precedentemente','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',133,1,'Otros servicios no personales no especificados precedentemente.'),(2016,4,0,0,0,'No',0,'BIENES DE USO','BIENES DE USO','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',134,1,'Gastos que se generan por la adquisición o construcción de bienes de capital que aumentan el activo de las jurisdicciones y entidades del Sector Público en un período dado; siendo éstos los bienes físicos, construcciones y/o equipos que sirven para producir otros bienes y servicios, no se agotan en el primer uso que de ellos se hace, tienen una duración superior a un año y están sujetos a depreciación. Incluye, asimismo, los activos intangibles. Deberán incluirse los gastos generados por la adquisición y construcción de bienes de uso propios y aquellos adquiridos o construidos para ser transferidos a terceros. \nTambién se incluyen los bienes de uso adquiridos bajo la modalidad “llave en mano” por la cual se efectúa un contrato de construcción a riesgo del contratista.'),(2016,4,1,0,0,'No',1,'BIENES PREEXISTENTES','Bienes preexistentes','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',135,1,'Comprende la adquisición de bienes físicos, ya existentes, que por sus características no pueden ser considerados dentro de las restantes partidas principales de este inciso.'),(2016,4,1,1,0,'Si',0,'TIERRAS Y TERRENOS','Tierras y terrenos','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',136,1,'Predios urbanos baldíos, campos con o sin mejoras. '),(2016,4,1,2,0,'Si',0,'EDIFICIOS E INSTALACIONES','Edificios e instalaciones','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',137,1,'Edificios en general, incluido el terreno en que se asientan, fábricas, represas, puentes, muelles, canalizaciones, redes de servicio público o privado y otros bienes de capital adheridos al terreno incluido este y los derechos de servidumbre '),(2016,4,1,3,0,'Si',0,'OTROS BIENES PREEXISTENTES','Otros bienes preexistentes','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',138,1,'Otros bienes de capital preexistentes no incluidos en algunas de las partidas parciales precedentes. '),(2016,4,2,0,0,'No',1,'CONSTRUCCIONES','Construcciones','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',139,1,'Comprende la realización de obras que permanecen con carácter de adherencia al suelo formando parte de un todo indivisible, como así también las ampliaciones mejorativas de construcciones ya existentes. Se consideran como tales caminos, diques, puentes, edificios, canales de riego, desagües o navegación, sistema de balizamiento, redes de comunicaciones, distribución de energía, de agua, fábricas, usinas, etc. no incluye el valor de la tierra, el que se preverá en la partida parcial Tierras y Terrenos. Comprende asimismo relevamientos cartográficos, geológicos, mineros, etc., necesarios para la construcción de un proyecto preconcebido en un área y con objetivos determinados. También se incluyen las obras que se realizan bajo la modalidad “llave en mano” por la cual se efectúa un contrato de construcción a riesgo del contratista.'),(2016,4,2,1,0,'Si',0,'CONST. EN BIENES DE DOM. PRIV.','Construcciones en bienes de dominio privado','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',140,1,'Gastos destinados a la construcción de obras del dominio privado tales como: edificios para oficinas públicas, edificaciones para salud, militares y de seguridad, educativas, culturales, para viviendas, para actividades comerciales, industriales y/o de servicios. '),(2016,4,2,2,0,'Si',0,'CONST.EN BIENES DE DOM. PUB','Construcciones en bienes de dominio publico','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',141,1,'Gastos destinados a la construcción de obras del dominio público tales como: calles, caminos y carreteras, plazas, canales, puentes y cualquier otra obra pública construida para utilidad o comodidad común. '),(2016,4,3,0,0,'No',1,'MAQUINARIA Y EQUIPO','Maquinaria y equipo','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',142,1,'Maquinarias, equipos y accesorios que se usan o complementan a la unidad principal, comprendiendo: maquinaria y equipo de oficina, de producción, equipos agropecuarios, industriales, de transporte en general, energía, riego, frigorífico, de comunicaciones, médicos, de recreación, educativos, etc. '),(2016,4,3,1,0,'Si',0,'MAQUINARIA Y EQUIPO DE PRODUCC','Maquinaria y equipo de produccion','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',143,1,'Maquinaria y equipo utilizados primordialmente en la industria de la construcción, en la producción agropecuaria, en las industrias manufactureras, en la producción de servicios (energía, gas, agua potable, etc.), entre otros. '),(2016,4,3,2,0,'Si',0,'EQ.DE TRANSP. TRACC. Y ELEV.','Equipo de transporte, traccion y elevacion','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',144,1,'Equipos de transporte por vía terrestre, fluvial, marítima, lacustre y aérea. Incluye asimismo equipos de tracción y elevación como: tractores, autoguías, montacargas, motoniveladoras, elevadores, ascensores, tráiler, entre otros.'),(2016,4,3,3,0,'Si',0,'EQUIPO SANITARIO Y DE LABORATO','Equipo sanitario y de laboratorio','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',145,1,'Equipos médicos, odontológicos, sanitarios y de investigación: comprende entre otros, mesas de operación, bombas de cobalto, aparatos de rayos X, tomógrafos, instrumental médico quirúrgico, microscopios, autoclaves, refrigeradores especiales, esterilizadores, balanzas de precisión, entre otros. '),(2016,4,3,4,0,'Si',0,'EQUIPO DE COMUNICACION Y SE├æAL','Equipo de comunicacion y se├▒alamiento','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',146,1,'Plantas transmisoras, receptoras de radio, equipo de televisión, aparatos telegráficos, teletipos, torres de transmisión, equipos utilizados en aeronavegación y marítimo, centrales y aparatos telefónicos y demás equipos de comunicación. Equipos de señalización: de rutas, de calles, boyas, balizas, entre otros. '),(2016,4,3,5,0,'Si',0,'EQUIPO EDUCACIONAL Y RECREATIV','Equipo educacional y recreativo','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',147,1,'Aparatos audiovisuales (proyectores, micrófonos, grabadores, televisores, etc.), muebles especializados para uso escolar (pupitres, pizarrones, etc.), equipos recreativos y deportivos (aparatos para parques infantiles, equipo para prácticas deportivas y gimnasia, mesas especiales de juegos en los casinos, billares, instrumentos musicales y otros elementos recreativos y deportivos). Otros equipos destinados a la educación y recreación. '),(2016,4,3,6,0,'Si',0,'EQUIPO PARA COMPUTACION','Equipo para computacion','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',148,1,'Unidades centrales de procesamiento, pantallas, impresoras, computadoras, unidades de cinta, unidades de disco, entre otros. '),(2016,4,3,7,0,'Si',0,'EQUIPO DE OFICINA Y MUEBLES','Equipo de oficina y muebles','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',149,1,'Mobiliario de distinta índole para oficinas y equipamiento tales como: estantes, escritorios, ficheros, percheros, mesas, máquinas de escribir, de sumar, de calcular, de contabilidad, de reproducción de copias, de aire acondicionado, refrigeradores, mesas para dibujo, cocinas, camas, sillas, entre otros.'),(2016,4,3,8,0,'Si',0,'HERRAMIENTAS Y REPUESTOS MAYOR','Herramientas y repuestos mayores','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',150,1,'Repuestos mayores que tienden a aumentar sustancialmente el valor del equipo o a prolongar su vida útil, tales como: motores, carrocerías, chasis, etc, y máquinas y herramientas para tornear, perforar, frezar, cepillar, taladrar, rectificar, estampar, prensar, clavar, engrapar y encolar. Máquinas eléctricas y de gas para soldadura autógena, dura y blanda. Herramientas con motor y de funcionamiento con aire comprimido. Partes y accesorios de las herramientas enunciadas.'),(2016,4,3,9,0,'Si',0,'EQUIPOS VARIOS','Equipos varios','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',151,1,'Otro tipo de maquinaria y equipo no incluido en las partidas parciales anteriores.'),(2016,4,4,0,0,'Si',0,'EQUIPO MILITAR Y DE SEGURIDAD','Equipo militar y de seguridad','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',152,1,'Equipamiento destinado al mantenimiento del orden público'),(2016,4,5,0,0,'Si',0,'LIB. REV. Y OT. ELEM. COLECC.','Libros, revistas y otros elementos coleccionables','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',153,1,'Adquisición de libros, revistas, mapas, películas cinematográficas impresas, discos fonoeléctricos y otros elementos destinados a la formación de colecciones.'),(2016,4,6,0,0,'Si',0,'OBRAS DE ARTE','Obras de arte','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',154,1,'Colecciones artísticas y ornamentales, tales como: pinturas, estatuas, tallas, antigüedades, entre otros'),(2016,4,7,0,0,'Si',0,'SEMOVIENTES','Semovientes','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',155,1,'Ganado de diferentes especies y todo tipo de animales adquiridos con fines de reproducción, trabajo u ornamento. '),(2016,4,8,0,0,'No',1,'ACTIVOS INTANGIBLES','Activos intangibles','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',156,1,'Adquisición de derechos por el uso de activos de la propiedad industrial, comercial, intelectual y otros.'),(2016,4,8,1,0,'Si',1,'PROGRAMAS DE COMPUTACI├ôN','Programas de computacion','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',157,1,'Gastos por programas, rutinas y su documentación completa asociada, los cuales pueden ser implementados en su sistema computacional. Incluye los gastos que se realizan en concepto de licencias por el uso de programas de computación.'),(2016,4,8,9,0,'Si',0,'OTROS ACTIVOS INTANGIBLES','Otros activos intangibles','Ingresado','1974-10-10','1974-10-10','PROD_ESIDIF','1974-10-10',158,1,'Otros activos intangibles no incluidos en la partida parcial anterior.');
/*!40000 ALTER TABLE `clasif_presup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documentation_type`
--

DROP TABLE IF EXISTS `documentation_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documentation_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abbreviation` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documentation_type`
--

LOCK TABLES `documentation_type` WRITE;
/*!40000 ALTER TABLE `documentation_type` DISABLE KEYS */;
INSERT INTO `documentation_type` VALUES (1,'PEDIDO AUTORIZADO','PEDA',NULL,NULL),(2,'PRESUPUESTOS DE PEDIDO','PRES',NULL,NULL),(3,'DOCUMENTACION TECNICA','DOCT',NULL,NULL),(4,'PDF FIRMA DIGITAL TECNICA','PEDF',NULL,NULL);
/*!40000 ALTER TABLE `documentation_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documentations`
--

DROP TABLE IF EXISTS `documentations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documentations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `documentation_type_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `type_file` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `directory_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `url_file` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documentations_user_id_foreign` (`user_id`),
  KEY `documentations_order_id_foreign` (`order_id`),
  KEY `documentations_documentation_type_id_foreign` (`documentation_type_id`),
  CONSTRAINT `documentations_documentation_type_id_foreign` FOREIGN KEY (`documentation_type_id`) REFERENCES `documentation_type` (`id`),
  CONSTRAINT `documentations_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  CONSTRAINT `documentations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documentations`
--

LOCK TABLES `documentations` WRITE;
/*!40000 ALTER TABLE `documentations` DISABLE KEYS */;
INSERT INTO `documentations` VALUES (18,2,1,'png','PRE2021041620',20,'pRBCBhNxkJQNnSwa12iXJsvAQkJOMVnWlOcvw3KI','2021-04-16 04:22:26','2021-04-16 04:22:26'),(19,2,1,'png','PRE2021041621',21,'bwDzfhGmuDw4psbir3McJi8JlGNmOmked2Qfag1G','2021-04-16 04:24:50','2021-04-16 04:24:50'),(20,2,1,'png','PRE2021041622',22,'qK8wsBRU2V3AdLs0cXgl2gkIKa8IWbMSLIMGdYbA','2021-04-16 04:25:10','2021-04-16 04:25:10'),(21,2,1,'png','PRE2021041623',23,'VEnTRMaCte3nIDXtIkj4DdfbwE1DRw2XxDDeSxgz','2021-04-16 04:28:20','2021-04-16 04:28:20');
/*!40000 ALTER TABLE `documentations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (37,'2014_10_12_000000_create_users_table',1),(38,'2014_10_12_100000_create_password_resets_table',1),(39,'2019_08_19_000000_create_failed_jobs_table',1),(40,'2021_02_01_223703_create_offices_table',1),(41,'2021_02_10_185659_create_buy_types_table',1),(42,'2021_03_15_174950_create_states_table',1),(43,'2021_03_25_1446348_create_buy_type_table',1),(44,'2021_03_25_184639_create_units_table',1),(45,'2021_03_26_143438_create_orders_table',1),(46,'2021_03_26_143555_create_order_detail_table',1),(47,'2021_03_30_214823_create_documentation_type_table',1),(48,'2021_03_31_212447_create_documentations_table',1),(49,'2021_03_31_212620_create_order_tracking_table',1),(50,'2021_04_01_220759_create_permissions_table',1),(51,'2021_04_01_221403_create_roles_table',1),(52,'2021_04_01_231214_create_permissions_users_table',1),(53,'2021_04_01_232436_create_roles_users_table',1),(54,'2021_04_01_232454_create_office_user_table',1),(55,'2021_04_15_183824_create_saf_office_table',1),(56,'2021_04_16_012735_create_buy_type_saf_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `office_user`
--

DROP TABLE IF EXISTS `office_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `office_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `office_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `office_user_user_id_foreign` (`user_id`),
  KEY `office_user_office_id_foreign` (`office_id`),
  CONSTRAINT `office_user_office_id_foreign` FOREIGN KEY (`office_id`) REFERENCES `offices` (`id`),
  CONSTRAINT `office_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `office_user`
--

LOCK TABLES `office_user` WRITE;
/*!40000 ALTER TABLE `office_user` DISABLE KEYS */;
INSERT INTO `office_user` VALUES (1,1,1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `office_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offices`
--

DROP TABLE IF EXISTS `offices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offices` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `saf_id` int(10) unsigned NOT NULL,
  `parent` bigint(20) unsigned DEFAULT NULL,
  `hierarchy_level` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `offices_parent_foreign` (`parent`),
  KEY `offices_saf_id_foreign` (`saf_id`),
  CONSTRAINT `offices_parent_foreign` FOREIGN KEY (`parent`) REFERENCES `offices` (`id`),
  CONSTRAINT `offices_saf_id_foreign` FOREIGN KEY (`saf_id`) REFERENCES `saf` (`id_saf`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offices`
--

LOCK TABLES `offices` WRITE;
/*!40000 ALTER TABLE `offices` DISABLE KEYS */;
INSERT INTO `offices` VALUES (1,'Gobierno',1,NULL,1,'2021-04-02 00:00:00','2021-04-02 00:00:00',NULL),(2,'Ministerio X',1,1,2,'2021-04-02 00:00:00','2021-04-02 00:00:00',NULL),(3,'Secretaria X',1,2,3,'2021-04-02 00:00:00','2021-04-02 00:00:00',NULL),(4,'Direccion X',1,3,4,'2021-04-02 00:00:00','2021-04-02 00:00:00',NULL),(5,'Ministerio Y',1,1,2,'2021-04-02 00:00:00','2021-04-02 00:00:00',NULL),(6,'Secretaría Y',1,5,3,'2021-04-02 00:00:00','2021-04-02 00:00:00',NULL),(7,'Secretaria Z',1,5,3,'2021-04-02 00:00:00','2021-04-02 00:00:00',NULL),(8,'Direccion Y',1,6,4,'2021-04-02 00:00:00','2021-04-02 00:00:00',NULL),(9,'Direccion Z',1,7,4,'2021-04-02 00:00:00','2021-04-02 00:00:00',NULL),(10,'Coordinacion X',1,9,5,'2021-04-02 00:00:00','2021-04-02 00:00:00',NULL);
/*!40000 ALTER TABLE `offices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_detail`
--

DROP TABLE IF EXISTS `order_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `exercise` int(11) NOT NULL,
  `subsection` int(11) NOT NULL,
  `principal` int(11) NOT NULL,
  `partial` int(11) NOT NULL,
  `sub_partial` int(11) NOT NULL,
  `budget_classification_id` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direction` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_description` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double(8,2) NOT NULL,
  `unit_id` int(10) unsigned NOT NULL,
  `estimated_price` double(8,2) NOT NULL,
  `total` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_detail_order_id_foreign` (`order_id`),
  KEY `order_detail_unit_id_foreign` (`unit_id`),
  CONSTRAINT `order_detail_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  CONSTRAINT `order_detail_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_detail`
--

LOCK TABLES `order_detail` WRITE;
/*!40000 ALTER TABLE `order_detail` DISABLE KEYS */;
INSERT INTO `order_detail` VALUES (16,20,2016,2,0,0,0,'1','asd','asd',12.00,1,12.00,144.00,'2021-04-16 04:22:26','2021-04-16 04:22:26'),(17,20,2016,2,0,0,0,'1','asddd','asd',32.00,1,12.00,384.00,'2021-04-16 04:22:26','2021-04-16 04:22:26'),(18,21,2016,2,0,0,0,'1','asdasd','sasd',11.00,1,32.00,352.00,'2021-04-16 04:24:50','2021-04-16 04:24:50'),(19,21,2016,2,0,0,0,'1','dasdasd','dsdas',12.00,1,12.00,144.00,'2021-04-16 04:24:50','2021-04-16 04:24:50'),(20,22,2016,2,0,0,0,'1','asdasd','sasd',11.00,1,32.00,352.00,'2021-04-16 04:25:10','2021-04-16 04:25:10'),(21,22,2016,2,0,0,0,'1','asda','dddaa',12.00,1,2.00,24.00,'2021-04-16 04:25:10','2021-04-16 04:25:10'),(22,23,2016,2,0,0,0,'1','asdasd','asdasd',32.00,1,1.00,32.00,'2021-04-16 04:28:20','2021-04-16 04:28:20'),(23,23,2016,2,0,0,0,'1','dddd','ddaddd',12.00,1,3.00,36.00,'2021-04-16 04:28:20','2021-04-16 04:28:20');
/*!40000 ALTER TABLE `order_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_tracking`
--

DROP TABLE IF EXISTS `order_tracking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_tracking` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `state_id` int(10) unsigned NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_tracking_order_id_foreign` (`order_id`),
  KEY `order_tracking_user_id_foreign` (`user_id`),
  KEY `order_tracking_state_id_foreign` (`state_id`),
  CONSTRAINT `order_tracking_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  CONSTRAINT `order_tracking_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`),
  CONSTRAINT `order_tracking_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_tracking`
--

LOCK TABLES `order_tracking` WRITE;
/*!40000 ALTER TABLE `order_tracking` DISABLE KEYS */;
INSERT INTO `order_tracking` VALUES (8,20,1,2,NULL,'2021-04-16 04:22:26','2021-04-16 04:22:26'),(9,21,1,2,NULL,'2021-04-16 04:24:50','2021-04-16 04:24:50'),(10,22,1,2,NULL,'2021-04-16 04:25:10','2021-04-16 04:25:10'),(11,23,1,2,NULL,'2021-04-16 04:28:20','2021-04-16 04:28:20'),(12,23,1,3,'Se solicita autorizacion','2021-04-16 04:33:36','2021-04-16 04:33:36'),(13,23,1,4,'Pedido Autorizado','2021-04-16 04:38:05','2021-04-16 04:38:05');
/*!40000 ALTER TABLE `order_tracking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `saf_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `office_id` bigint(20) unsigned NOT NULL,
  `buy_type_id` int(10) unsigned DEFAULT NULL,
  `total` double(8,2) NOT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_characteristic` int(11) DEFAULT NULL,
  `applicant` int(11) DEFAULT NULL,
  `file_number_physical` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `canceled_reason` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `canceled_by_user` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_user_id_foreign` (`user_id`),
  KEY `orders_saf_id_foreign` (`saf_id`),
  KEY `orders_office_id_foreign` (`office_id`),
  KEY `orders_buy_type_id_foreign` (`buy_type_id`),
  CONSTRAINT `orders_buy_type_id_foreign` FOREIGN KEY (`buy_type_id`) REFERENCES `buy_types` (`id`),
  CONSTRAINT `orders_office_id_foreign` FOREIGN KEY (`office_id`) REFERENCES `offices` (`id`),
  CONSTRAINT `orders_saf_id_foreign` FOREIGN KEY (`saf_id`) REFERENCES `saf` (`id_saf`),
  CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (20,1,1,1,1,528.00,'prueba 800000',0,NULL,NULL,NULL,NULL,'2021-04-16 04:22:26','2021-04-16 04:22:26',NULL),(21,1,1,1,1,496.00,'prueba sin transaccion',0,NULL,NULL,NULL,NULL,'2021-04-16 04:24:50','2021-04-16 04:24:50',NULL),(22,1,1,1,1,376.00,'prueba sin transaccion',0,NULL,NULL,NULL,NULL,'2021-04-16 04:25:10','2021-04-16 04:25:10',NULL),(23,1,1,1,1,68.00,'prueba sin caracter',NULL,NULL,NULL,NULL,NULL,'2021-04-16 04:28:20','2021-04-16 04:28:20',NULL);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_user`
--

DROP TABLE IF EXISTS `permission_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `permission_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_user_user_id_foreign` (`user_id`),
  KEY `permission_user_permission_id_foreign` (`permission_id`),
  CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`),
  CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_user`
--

LOCK TABLES `permission_user` WRITE;
/*!40000 ALTER TABLE `permission_user` DISABLE KEYS */;
INSERT INTO `permission_user` VALUES (1,1,1,'2021-04-02 00:00:00','2021-04-02 00:00:00',NULL),(2,1,2,'2021-04-02 00:00:00','2021-04-02 00:00:00',NULL),(3,1,3,'2021-04-02 00:00:00','2021-04-02 00:00:00',NULL),(4,1,4,'2021-04-02 00:00:00','2021-04-02 00:00:00',NULL),(5,1,5,'2021-04-02 00:00:00','2021-04-02 00:00:00',NULL);
/*!40000 ALTER TABLE `permission_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descriptions` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `operation_type` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'Consultar usuarios','Permite a un Usuario ver el listado de usuarios.','users','R','2021-04-02 00:00:00','2021-04-02 00:00:00',NULL),(2,'Consultar permisos','Permite a un Usuario ver el listado de permisos asignados a otro usuario.','permissions_users','R','2021-04-02 00:00:00','2021-04-02 00:00:00',NULL),(3,'Asignar permisos','Permite a un Usuario agregar permisos a otro usuario','permissions_users','C','2021-04-02 00:00:00','2021-04-02 00:00:00',NULL),(4,'Denegar permisos','Permite a un Usuario denegar un permiso asignado anteriormente.','permissions_users','D','2021-04-02 00:00:00','2021-04-02 00:00:00',NULL),(5,'autorizar pedido ','el usuario puede autorizar los pedidos ','Order','A',NULL,NULL,NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (1,1,2,NULL,NULL,NULL);
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Usuario Comun','2021-04-02 00:00:00','2021-04-02 00:00:00',NULL),(2,'Director','2021-04-02 00:00:00','2021-04-02 00:00:00',NULL),(3,'Autorizador','2021-04-02 00:00:00','2021-04-02 00:00:00',NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `saf`
--

DROP TABLE IF EXISTS `saf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saf` (
  `EJER` decimal(4,0) DEFAULT NULL COMMENT 'Año del Ejercicio',
  `S1` smallint(1) DEFAULT NULL COMMENT 'Identificador del Sector',
  `SECTOR` varchar(40) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Ubicacion dentro del Estado Provincial',
  `S2` smallint(1) DEFAULT NULL COMMENT 'Identificador del Sub Sector',
  `SUBSECTOR` varchar(40) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Entidades del Estado',
  `S3` smallint(1) DEFAULT NULL COMMENT 'Identificador del Carácter',
  `CARACTER` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Descripcion de Organismos',
  `JUR` smallint(3) DEFAULT NULL COMMENT 'Identificador de la Juridiccion',
  `JURISDICCION` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Organismo Publico sin Personalidad Juridica',
  `SUBJUR` smallint(1) DEFAULT NULL COMMENT 'Identificador de las Sub Juridiccion',
  `SUBJURISDICCION` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Dependencias de la Juridiccion',
  `ENTIDAD` smallint(4) DEFAULT NULL COMMENT 'Identificador de Organismos Descentralizados y de Seguridad Social',
  `SERVICIO` smallint(4) DEFAULT NULL COMMENT 'Codigo del SAF',
  `icon_saf` char(15) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Ruta Imagen GIF de SAF',
  `NOMBRE` varchar(120) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Denominacion del SAF',
  `id_saf` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificar Auto Incremental',
  `saf_nombre_abreviatura` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Abreviacion de la Denominacion del SAF',
  `baja` smallint(1) DEFAULT '0',
  PRIMARY KEY (`id_saf`) USING BTREE,
  KEY `ejercicio` (`EJER`) USING BTREE,
  KEY `SERVICIO` (`SERVICIO`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=212 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `saf`
--

LOCK TABLES `saf` WRITE;
/*!40000 ALTER TABLE `saf` DISABLE KEYS */;
INSERT INTO `saf` VALUES (2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',1,'FUNCION LEGISLATIVA',0,'',0,1,'img/saf_001.gif','Funcion Legislativa',1,'Funcion Legislativa',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',2,'FUNCION JUDICIAL',0,'',0,2,'img/saf_002.gif','Funcion Judicial',2,'Funcion Judicial',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',3,'TRIBUNAL DE CUENTAS',0,'',0,3,'img/saf_003.gif','Tribunal de Cuentas',3,'Tribunal de Cuentas',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',1,'FUNCION LEGISLATIVA',0,'',0,5,'img/saf_005.gif','Funcion Legislativa - Vice-Presidencia Primera',5,'Funcion Legislativa - Vice-Presidencia 1ra.',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',10,'FUNCION EJECUTIVA',1,'',0,110,'img/saf_010.gif','Gobernacion',6,'Gobernacion',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',10,'FUNCION EJECUTIVA',1,'SECRETARIA GENERAL Y LEGAL DE LA GOBERNACION',0,115,'img/saf_015.gif','Coordinacion Ejecutiva Provincial',7,'Coordinacion Ejecutiva Provincial',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',10,'FUNCION EJECUTIVA',3,'SECRETARIA DE CULTURA',0,130,'img/saf_130.gif','Secretaria de Culturas',9,'Sec. de Culturas',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',10,'FUNCION EJECUTIVA',4,'SECRETARIA DE TURISMO',0,140,'img/saf_140.gif','Turismo',10,'Turismo',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',10,'FUNCION EJECUTIVA',5,'ENTIDAD DE ENLACE - PROSAP',0,150,'img/saf_150.gif','Entidad de Enlace',11,'Entidad de Enlace',1),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',2,'ORGANISMOS DESCENTRALIZADOS',10,'FUNCION EJECUTIVA',6,'ADMINISTRACIONES PROVINCIALES',161,161,'img/saf_161.gif','Ente Unico de Control de Privatizaciones',12,'EUCoP',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',20,'MINISTERIO DE DESARROLLO SOCIAL',0,'',0,200,'img/saf_200.gif','Min. de Desarrollo, Igualdad e Integracion Social',13,'Desarrollo, Igualdad e Integracion Social',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',25,'MINIST.DE GOBIERNO, JUSTICIA, SEGURIDAD Y DERECHOS HUMANOS',0,'',0,250,'img/saf_250.gif','Gobierno, Justicia, Seguridad y DD HH',14,'Gobierno, Justicia, Seguridad y DD HH',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',25,'MINIST.DE GOBIERNO, JUSTICIA, SEGURIDAD Y DERECHOS HUMANOS',0,'',0,255,'img/saf_255.gif','Trabajo',15,'Trabajo',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',25,'MINIST.DE GOBIERNO, JUSTICIA, SEGURIDAD Y DERECHOS HUMANOS',0,'',0,256,'img/saf_256.gif','Policia de la Provincia',16,'Policia',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',30,'MINISTERIO DE HACIENDA',0,'',0,300,'img/saf_300.gif','Ministerio de Hacienda',17,'Hacienda',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',2,'ORGANISMOS DESCENTRALIZADOS',30,'MINISTERIO DE HACIENDA',0,'',301,301,'img/saf_301.gif','Juegos de Azar de La Rioja',18,'AJALaR',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',40,'MINISTERIO DE EDUCACION, CIENCIA Y TECNOLOGIA',0,'',0,400,'img/saf_400.gif','Min. de Educacion, Ciencia y Tecnologia',19,'Educacion, Ciencia y Tecnologia',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',40,'MINISTERIO DE EDUCACION, CIENCIA Y TECNOLOGIA',0,'',0,420,'img/saf_420.gif','Pacto Federal',20,'Pacto Federal',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',50,'MINISTERIO DE SALUD PUBLICA',0,'',0,500,'img/saf_500.gif','Min. de Salud Publica',21,'Salud Publica',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',3,'INSTITUCIONES SEGURIDAD SOCIAL',50,'MINISTERIO DE SALUD PUBLICA',0,'',501,501,'img/saf_501.gif','Administracion Provincial de Obra Social',22,'APOS',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',50,'MINISTERIO DE SALUD PUBLICA',0,'',0,510,'img/saf_510.gif','Hospital Enrique Vera Barros',23,'Hospital Enrique Vera Barros',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',60,'MINISTERIO DE PRODUCCION Y DESARROLLO LOCAL',0,'',0,600,'img/saf_600.gif','Min. de Produccion y Des. Economico',25,'Produccion y Desarrollo Economico',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',60,'MINISTERIO DE PRODUCCION Y DESARROLLO LOCAL',0,'',0,610,'img/saf_610.gif','Sec. de Agricultura',26,'Secretaria Agricultura y Rec.Naturales',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',60,'MINISTERIO DE PRODUCCION Y DESARROLLO LOCAL',0,'',0,620,'img/saf_620.gif','Sec. de Ganaderia',27,'Ganaderia',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',70,'MINISTERIO DE INFRAESTRUCTURA',0,'',0,700,'img/saf_700.gif','Ministerio de Infraestructura',28,'Infraestructura',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',2,'ORGANISMOS DESCENTRALIZADOS',70,'MINISTERIO DE INFRAESTRUCTURA',0,'',701,701,'img/saf_701.gif','Adm. Prov. de Vivienda y Urbanismo',29,'Secretaria de Vivienda',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',2,'ORGANISMOS DESCENTRALIZADOS',70,'MINISTERIO DE INFRAESTRUCTURA',0,'',702,702,'img/saf_702.gif','Adm. Prov. de Vialidad',30,'Vialidad',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',70,'MINISTERIO DE INFRAESTRUCTURA',0,'',0,640,'img/saf_640.gif','Sec. de Tierra y Habitat Social',31,'Tierra y Habitat Social',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',70,'MINISTERIO DE INFRAESTRUCTURA',0,'',0,730,'img/saf_730.gif','Inversion Publica Provincial',32,'Inversion Publica Provincial',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',70,'MINISTERIO DE INFRAESTRUCTURA',0,'',0,740,'img/saf_740.gif','Sec. de Transporte, Transito y Seguridad Vial',33,'Transporte, Transito y Seguridad Vial',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',90,'SERVICIO DE LA DEUDA',0,'',0,900,'img/saf_900.gif','Dir. Gral. de Deuda Publica',34,'Deuda Publica',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',91,'OBLIGACIONES A CARGO DEL TESORO',0,'',0,910,'img/saf_910.gif','Dir. de Obligaciones a Cargo del Tesoro',35,'Obligaciones a Cargo del Tesoro',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',92,'RECURSOS DEL TESORO PROVINCIAL',0,'',0,992,'img/saf_992.gif','Recursos del Tesoro Provincial',36,'Recursos del Tesoro Provincial',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',2,'ORGANISMOS DESCENTRALIZADOS',70,'MINISTERIO DE INFRAESTRUCTURA',0,'',703,703,'img/saf_703.gif','Instituto Provincial del Agua',38,'IPALaR',1),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',2,'ORGANISMOS DESCENTRALIZADOS',10,'FUNCION EJECUTIVA',6,'ADMINISTRACIONES PROVINCIALES',164,164,'img/saf_164.gif','Adm. de Radio y TV Riojana',39,'Radio y TV Riojana',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',60,'MINISTERIO DE PRODUCCION Y DESARROLLO LOCAL',0,'',0,630,'img/saf_630.gif','Sec. de Mineria y Energia',40,'Mineria y Energia',0),(2013,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',30,'MINISTERIO DE HACIENDA',0,'',0,330,'img/saf_330.gif','Dir. de Relaciones con Municipios',41,'Relaciones con Municipios',0),(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,650,'img/saf_650.gif','Planeamiento e Industria',42,'Planeamiento e Industria',0),(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,660,'img/saf_660.gif','Sec. de Integracion Regional y Coop. Internacional',43,'Sec. de Integracion Reg. y Coop. Int.',0),(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,180,'img/saf_180.gif','Relaciones Institucionales y Politicas Regionales',44,'Rel. Institucionales y Politicas Regionales',0),(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,502,'img/saf_502.gif','Hospital Madre y el Ni&ntilde;o',45,'Hospital Madre y el Ni&ntilde;o',0),(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,181,'img/saf_181.gif','Unidad de Proyectos y Obras',46,'Proyectos y Obras',0),(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,182,'img/saf_182.gif','Agencia de Espacios Publicos y Eventos',47,'Agencia de Espacios Publicos y Eventos',0),(2018,1,'PUBLICO PCIAL. NO FINANCIERO',1,'ADMINISTRACION PROVINCIAL',1,'ADMINISTRACI-N CENTRAL',10,'FUNCION EJECUTIVA',8,'Otras Secretarias y Organismos',NULL,183,'img/saf_183.gif','Secretaria de Economia Social y Popular',48,'Secretaria de Economia Social y Popular',0),(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,184,'img/saf_184.gif','Unidad de Gestion Integral de Fortalecimiento Institucional y Socio Comunitaria',49,'Gestion Integral de Fortalec. Inst. y Socio Com.',0),(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Proveedor',200,NULL,0),(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,615,'img/saf_615.gif','Entidad de Enlace',202,'Entidad de Enlace',0),(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,40,'img/saf_004.gif','Fiscalia General',203,'Fiscalia',0),(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,41,'img/saf_004.gif','Defensoria General',204,'Defensoria',0),(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,111,'img/saf_111.gif','Secretaria de Comunicacion y Planificacion Publica',205,'Secretaria de Comunicacion',0),(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,112,'img/saf_112.gif','Jefatura de Gabinete',206,'Jefatura de Gabinete',0),(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,113,'img/saf_113.gif','Ministerio de Turismo y Culturas',207,'Ministerio de Turismo y Culturas',0),(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,711,'img/saf_711.gif','Ministerio de Vivienda, Tierras y Habitad Social',208,'Ministerio de Vivienda, Tierras y Habitad Social',0),(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,712,'img/saf_712.gif','Ministerio de Agua y Energia',209,'Ministerio de Agua y Energia',0),(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,165,'img/saf_165.gif','Instituto Regional de Planificacion, Control y Servicios Ambientales - IREPCSA',210,'IREPCSA',0),(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,258,NULL,'Secretaria de Justicia',211,'Secretaria de Justicia',0);
/*!40000 ALTER TABLE `saf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `saf_office`
--

DROP TABLE IF EXISTS `saf_office`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saf_office` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `saf_id` int(10) unsigned DEFAULT NULL,
  `office_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `saf_office_saf_id_foreign` (`saf_id`),
  KEY `saf_office_office_id_foreign` (`office_id`),
  CONSTRAINT `saf_office_office_id_foreign` FOREIGN KEY (`office_id`) REFERENCES `offices` (`id`),
  CONSTRAINT `saf_office_saf_id_foreign` FOREIGN KEY (`saf_id`) REFERENCES `saf` (`id_saf`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `saf_office`
--

LOCK TABLES `saf_office` WRITE;
/*!40000 ALTER TABLE `saf_office` DISABLE KEYS */;
INSERT INTO `saf_office` VALUES (1,1,1,NULL,NULL);
/*!40000 ALTER TABLE `saf_office` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `states`
--

DROP TABLE IF EXISTS `states`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `states` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `states`
--

LOCK TABLES `states` WRITE;
/*!40000 ALTER TABLE `states` DISABLE KEYS */;
INSERT INTO `states` VALUES (1,'ANULADO'),(2,'GENERADO'),(3,'ESPERANDO AUTORIZACIÓN'),(4,'AUTORIZADO'),(5,'GENERACIÓN DE PLIEGO'),(6,'PLIEGO GENERADO'),(7,'FINALIZADO'),(8,'EN TRATAMIENTO'),(9,'EN ESPERA');
/*!40000 ALTER TABLE `states` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `units`
--

DROP TABLE IF EXISTS `units`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `units` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `units`
--

LOCK TABLES `units` WRITE;
/*!40000 ALTER TABLE `units` DISABLE KEYS */;
INSERT INTO `units` VALUES (1,'KILOS',NULL,NULL),(2,'MRETROS',NULL,NULL),(3,'LITROS',NULL,NULL);
/*!40000 ALTER TABLE `units` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Mr. Ken Zulauf','kuphal.gordon@example.net','2021-04-16 03:14:01','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','UnV2qoY7pL','2021-04-16 03:14:01','2021-04-16 03:14:01'),(2,'Petra Feest','cassin.eleanore@example.net','2021-04-16 03:14:01','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','OTmXk4O82A','2021-04-16 03:14:01','2021-04-16 03:14:01'),(3,'Dr. Jadon Streich','janis84@example.com','2021-04-16 03:14:01','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','3FyhbhMSi4','2021-04-16 03:14:01','2021-04-16 03:14:01'),(4,'Prof. Braden Fahey Jr.','ritchie.willy@example.com','2021-04-16 03:14:01','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','pW1d3KOcG0','2021-04-16 03:14:01','2021-04-16 03:14:01'),(5,'Marielle Hoppe','carlie.wuckert@example.com','2021-04-16 03:14:01','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','lgYKlpfGh7','2021-04-16 03:14:01','2021-04-16 03:14:01'),(6,'Mrs. Lottie Bogisich','deven08@example.net','2021-04-16 03:14:01','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','Gd6kpE7aoe','2021-04-16 03:14:01','2021-04-16 03:14:01'),(7,'Prof. Akeem Baumbach PhD','labadie.jazmin@example.com','2021-04-16 03:14:01','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','jxbHyKYbUV','2021-04-16 03:14:01','2021-04-16 03:14:01'),(8,'Antonietta Collier','jonatan.pfeffer@example.com','2021-04-16 03:14:01','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','bN3lRp07OF','2021-04-16 03:14:01','2021-04-16 03:14:01'),(9,'Frederic Wisoky','ernie.kuphal@example.org','2021-04-16 03:14:01','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','XGBanXsg7I','2021-04-16 03:14:01','2021-04-16 03:14:01'),(10,'Brooke Dietrich Jr.','jacques.bayer@example.net','2021-04-16 03:14:01','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','qRVWkrZIGy','2021-04-16 03:14:01','2021-04-16 03:14:01'),(11,'9No3uqPN7x','dZaoCzLaeq@gmail.com',NULL,'$2y$10$t93glLzd5xwpOKe.4mtseOSM29PhRa2cKQ3nHKr3x2.euVjtLapNG',NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-16  6:18:23
